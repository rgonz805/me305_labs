"""!
@file ME305_lab01.py
@brief run a program in a loop for three different patterns for a LED
@details This code starts with the light not blinking, then it starts with a
         pattern of blinking with patters according to their states. The states 
         are state 1, state 2 and state 3. These states represent a pattern 
         corresponding to a sinewave , a square wave and a sawtooth wave 
         respectively. 

        See Video Demo here:
        https://drive.google.com/file/d/16fxc6qqa4Gji9HpZnZ1hJuJmTU-Jp4-m/view?usp=sharing

@author Erick Daza
@author Rodrigo Gonzalez
@date 01-20-22
"""

import math
import time
import pyb
# import math, time and pyb moduels for future use

## @brief defines pinA5
#  @details uses pyb to call out pinA5 and recognizes it as the pin being use
#
#
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## @brief defines tim2
#  @details uses pyb to use the timer and set a frequency 
#
#
tim2 = pyb.Timer(2, freq = 20000)

## @brief defines t2ch1
#  @details Uses the time and channel to run PWM to pin A5
#
#
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## @brief defines pinC13
#  @details uses pyb to call out pinC13 and recongnizes it as the pin being use 
#
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

## @brief set the initial state to 0
#  @details set the state to 0 so the LED will not be blinking
#
#
state = 0


def onButtonPressFCN(IRQ_src):
    '''! @brief creates a function onButtonPressFCN
         @details the fucntion defined enables the IRQ interrupt to change 
         @param IRQ_src which is the action of pressing the button
         @return onButtonPressed = True
    
    '''  

    ## @brief set onButtonPress to a global variable
    #  @details onButtonPress is use to recongize a user input which chages
    #   between True and False when button is press or not press
    #
    #
    global state
    state= state + 1
    # set the current state plus one

def SineWave(timeSinceStart):
    '''! @brief creates a function SineWave
         @details math module is used to create a sine wave that can 
             be modified
         @param timeSinceStart that is a time variable
         @return fractions of numbers between 0 and 1
    
    ''' 
    
    ## @brief set brightneess
    #  @details brightness is manipulated to obtain the pattern desired
    #
    #
    brightness = abs(math.sin(timeSinceStart*2*3.14/10))
    
    return brightness

def SquareWave(timeSinceStart):
    '''! @brief creates a function SquareWave
         @details the fucntion uses a round and remainder function to get
             values 0 or 1 
         @param timeSinceStart that is a time variable
         @return 0 or 1
    
    ''' 
    
    ## @brief set brightneess
    #  @details brightness is manipulated to obtain the pattern desired
    #
    #
    brightness = (round(timeSinceStart,0)%2)*100
    
    return brightness

def SawToothWave(timeSinceStart):
    '''! @brief creates a function SawToothWave
         @details the fucntion uses math floor to get small increments until 1
         @param timeSinceStart that is a time variable
         @return small increment to reach the value of 1
    
    ''' 
    
    ## @brief set brightneess
    #  @details brightness is manipulated to obtain the pattern desired
    #
    #
    brightness =math.floor((timeSinceStart-math.floor(timeSinceStart))*100)
    
    return brightness
   
if __name__ == '__main__':    
 
 ## @brief set timer
 #  @details timer is set to the value of zero
 #
 #
 timer=0

 ## @brief set start time
 #  @details start time is set to the value of zero
 #
 #
 start_time=0

## @brief creates variable call ButtonInt
#  @details creates an interrupt variable that uses pyb mod to recieve the 
#   the input from pinC13
#
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)        



printstate1=True
printstate2=True
printstate3=True

print('Welcome! Press the blue button to shuffle through different wave patterns. The first wave pattern currently set is the Square Wave')
while True:
    try:
    #an infinite loop that runs until the condition is met
        if state == 0 :
        # if statement for button state 0
        
            ## @brief creates variable call timer
            #  @details gives timer a value using time.ticks as a reference of 
            #   time
            #
            timer= time.ticks_diff(time.ticks_ms(), start_time)/1000
            
            ## @brief set brightness 
            #  @details set the brightness intensity based on values generated
            #   by the square wave
            #
            brightness=SquareWave(timer)
            t2ch1.pulse_width_percent(brightness)
            if printstate1 == True :
               print('Square Wave Pattern Selected')
               printstate1=False
        elif state==1:
        # if statement for button state 1
        
            ## @brief changes the values for timer
            #  @details gives timer a value using time.ticks as a reference of 
            #   time
            #
            timer= time.ticks_diff(time.ticks_ms(), start_time)/1000
            
            ## @brief set brightness 
            #  @details set the brightness intensity based on values generated
            #   by the sine wave
            #
            brightness=SineWave( timer )*100
            t2ch1.pulse_width_percent(brightness)
            if printstate2 == True :
               print('Sine Wave Pattern Selected')
               printstate2=False
        elif state == 2:
        # if statement for button state 2    
            
            ## @brief set brightness
            #  @details set the brightness intensity based on values generated
            #   by the sawtooth wave
            #
            brightness=SawToothWave(timer)
            t2ch1.pulse_width_percent(brightness)
            
            ## @brief changes the values for timer
            #  @details gives timer a value using time.ticks as a reference of 
            #   time
            #
            timer= time.ticks_diff(time.ticks_ms(), start_time)/1000
            if printstate3 == True :
               print('Saw Tooth Wave Pattern Selected')
               printstate3=False
        else:
            state=0
            printstate1=True
            printstate2=True
            printstate3=True
        time.sleep(0.1)
    except KeyboardInterrupt:
        print('Terminating Program')
        break