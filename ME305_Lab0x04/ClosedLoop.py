'''!
@file ClosedLoop.py
@brief Controlls motor duty cycle and velocity
@details this  code creates a class to control a DC motor duty cycle, velocity and gain.

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-24-22
'''

class ClosedLoop:
    '''!@brief A closeloop class to use in the controller
        @details Objects of this class can be used to set gain, reference velocity,
                 calculate the error and motor actuation to a given DC motor.
    '''
    def __init__(self,gain,ref_vel):
        '''! @brief defines the _init_ function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self, gain, and ref_vel

        '''
         
        self.gain=gain
        self.ref_vel=ref_vel
        self.upperlimit=185
        self.lowerlimit=-185
       
    def set_Gain(self,Gain):
        '''! @brief defines the set_Gain function
             @details this function will run set the motor gain
             @param gain

        '''
        
        self.gain=Gain
    
    def set_ref_vel(self,RefVel):
        '''! @brief defines the set_ref_vel function
             @details this function will run set the motor reference velocity based on 
                      upper and lower limits stablished
             @param RefVel

        '''
        self.ref_vel = RefVel
        if self.ref_vel>=self.upperlimit:
            self.ref_vel=self.upperlimit
        if self.ref_vel<=self.lowerlimit:
            self.ref_vel=self.lowerlimit
            
    def error_calc(self,EncVel2):
        '''! @brief defines the error_calc function
             @details this function will calculate the error fedback signal
             @param EncVel2

        '''
        self.EncVel2 = EncVel2
        self.error=self.ref_vel-self.EncVel2
        if self.error>1:
            self.error=1
        elif self.error<-1:
            self.error=-1
            
    def Actuation(self,L):
        '''! @brief defines the Actuation function
             @details this function controlls the motor actuation
             @param L

        '''
        self.L= L
        self.L +=self.gain*(self.error)
        if self.L >= 100:
            self.L=100
        elif self.L <= -100:
            self.L=-100
        
        return self.L
