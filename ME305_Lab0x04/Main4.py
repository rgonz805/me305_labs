"""!
@file Main4.py
@brief run a program to regulate the speed and duty cycle of the motors
@details This code compiles all the information gathered by other files and 
         compiles and assigns it to task to control assure continuous gathering 
         and flow of information


@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

import Shares
import EncoderTask, UserTask, MotorTask, ControllerTask
# import Shares, EncoderTask and UserTask moduels for future use

## @brief defines zFlag
#  @details uses the file Shares and Shares class to set zFlag to False
#
#
zFlag = Shares.Share(False)

## @brief defines EncPosition
#  @details uses the file Shares and Shares class to set EncPositon to zero
#
#
EncPosition = Shares.Share(0)

## @brief defines EncDelta
#  @details uses the file Shares and Shares class to set EncDelta to zero
#
#
EncDelta = Shares.Share(0)

## @brief defines Duty1
#  @details uses the file Shares and Shares class to set Duty1 to zero
#
#
Duty1 = Shares.Share(0)

## @brief defines Duty2
#  @details uses the file Shares and Shares class to set Duty2 to zero
#
#
Duty2 = Shares.Share(0)

## @brief defines EncVel
#  @details uses the file Shares and Shares class to set EncVel to zero
#
#
EncVel = Shares.Share(0)

## @brief defines EncVel2
#  @details uses the file Shares and Shares class to set EncVel2 to zero
#
#
EncVel2 = Shares.Share(0)

## @brief defines Gain
#  @details uses the file Shares and Shares class to set the Gain to zero
#
#
Gain = Shares.Share(0)

## @brief defines Ref_vel
#  @details uses the file Shares and Shares class to set Ref_vel to zero
#
#
Ref_vel = Shares.Share(0)

## @brief defines cFlag
#  @details uses the file Shares and Shares class to set cFlag to False
#
#
cFlag = Shares.Share(False)

## @brief defines wFlag
#  @details uses the file Shares and Shares class to set wFlag to False
#
#
wFlag = Shares.Share(False)

## @brief defines ActLev
#  @details uses the file Shares and Shares class to set ActLev to zero
#
#
ActLev = Shares.Share(0)


if __name__ == '__main__':
    
    ## @brief defines task1
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, EncVel, Duty1, Duty2, and cFlagas as
    #           parameters and stores the result in task1
    task1 = UserTask.UserFunction('Task User', 10_000, zFlag,EncPosition, EncDelta, Duty1, Duty2, cFlag, EncVel, EncVel2,wFlag, Ref_vel, Gain, ActLev)
    
    ## @brief defines task2
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, and EncVel as parameters and stores the
    #           result in task2
    task2 = EncoderTask.EncoderFunction('Task Encoder', 10_000, zFlag, EncPosition, EncDelta, EncVel, EncVel2)
    
    ## @brief defines task3
    #  @details uses UserTask file and UserFunction with 10,000, cFlag, 
    #           Duty1, and Duty2 as parameters and stores the result in task3
    #           
    task3 = MotorTask.MotorFunction('Task Motor', 10_000 ,cFlag, Duty1, Duty2)
    
    task4 = ControllerTask.ControllerFunction('Task Controller' , 10_000, Gain, Ref_vel, EncVel2 ,wFlag, Duty1, ActLev)
    
    ## @brief defines taskList
    #  @details puts task1, task2 and task3 into a list for later use
    #
    #
    taskList = {task1, task2, task3, task4}
   
    while True:
        try:
        # an infinity loop that cycles between task1, task2 and task3
            for task in taskList:
                next(task1)
                next(task2)
                next(task3)
                next(task4)
       
        except KeyboardInterrupt:
            break
           
print('Terminating Program')
