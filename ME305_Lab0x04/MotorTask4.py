"""!
@file MotorTask4.py
@brief run a program that uses the information DRV8847 and Shares
@details This code uses the information from the driver, shares, cFlags to 
         run the motor with the appropriate speed or stop the motor when a fault 
         is detected

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

from pyb import Pin
from time import ticks_us,ticks_add, ticks_diff
import DRV8847
import Shares
import micropython
# imports Pin, ticks_us, ticks_add, ticks_diff, DRV8847, Shares, and micropython for later use

## @brief defines State0
#  @details uses micropython to assing the constant 0 to State0
#
#
State0 = micropython.const(0)

## @brief defines State1
#  @details uses micropython to assing the constant 1 to State1
#
#
State1 = micropython.const(1)

## @brief defines motor_drv
#  @details uses DRV8847 to store its information into this variable
#
#
motor_drv = DRV8847.DRV8847()

def MotorFunction(taskName,Period, cFlag, Duty1, Duty2):
    '''! @brief defines the _init_ function
         @details this function will run an provide information using the
                  parameters listed below.
         @param self.timer, self.nsleep, self.faultInt

    '''
    
    ## @brief defines state
    #  @details set state to State0
    #
    #
    state=State0

    ## @brief defines motor_1
    #  @details uses motor_drv to assingned motor1 the correct pins
    #
    #
    motor_1 = motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    
    ## @brief defines motor_2
    #  @details uses motor_drv to assingned motor2 the correct pins
    #
    #
    motor_2 = motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
       
    motor_drv.enable()
    
    ## @ brief defines time_current 
    #  @ details uses ticks_us to assing a time in milliseconds
    #
    #
    time_current=ticks_us()
    
    ## @ brief defines time_future
    #  @ details uses ticks_us to assing a time in milliseconds by adding time 
    #            ellapse to current time
    #
    time_future=ticks_add(time_current,Period)

    while True:
        time_passing=ticks_us()
   
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,Period)
           
            if state == State0:
                motor_1.set_duty(Duty1.read())
                motor_2.set_duty(Duty2.read())
               
                if cFlag.read():
                    state=State1

            elif state == State1:
                motor_drv.enable()
                print('Fault Cleared')
                cFlag.write(False)
                state = State0
        else:
            yield None