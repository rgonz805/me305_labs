"""!
@file Encoder4.py
@brief This code set encoder functions and creates the Encoder class
@details This code creates the Encoder class and subsequent function which cand b
         be use later in EncoderTask

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-017-22
"""

import pyb
# imports pyb module for future use

class Encoder:
    '''!@brief A Encoder class for use in EncoderTask.
        @details Objects of this class can be used to apply PWM to a given
                 encoder.

    '''
    def __init__(self,timer,pinnumber,channel1,channel2,):
        '''! @brief defines the _init_ function
             @details this function will run an collect information using the
                      parameters listed below.
             @param self, time, pinnumber, channel1, channel2

        '''
        ## @brief defines self.TIMER
        #  @details uses pyb.Timer to set period and prescaler
        #
        #
        self.TIMER = pyb.Timer(timer, prescaler=0, period=65535)
        
        ## @brief defines self.channel
        #  @details uses self.TIMER to update the information in channel 1
        #
        #
        self.channel = self.TIMER.channel(1,pyb.Timer.ENC_AB)  
        
        ## @brief defines self.pina
        #  @details uses pyb.Pin to set pin for AF_PP
        #
        #
        self.pina =pyb.Pin(channel1, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnumber)
        
        ## @brief defines self.pinb
        #  @details uses pyb.Pin to set pin for AF_PP
        #
        #
        self.pinb =pyb.Pin(channel2, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnumber)

        ## @brief defines self.EncPosition
        #  @details set EncPosition to zero
        #
        #
        self.EncPosition = 0
        
        ## @brief defines self.Position2
        #  @details set Position2 to zero
        #
        #
        self.Position2 = 0
        
        ## @brief defines self.Position1
        #  @details set Position1 to zero
        #
        #
        self.Position1 = 0
        
        ## @brief defines self.Delta
        #  @details set Delta to zero
        #
        #
        self.Delta = 0
       
    def update(self):
        '''! @brief defines the update function
             @details this function will continously update the positon of the
                      motor
             @param self

        '''
        AR = 65535
        self.Position1=self.Position2
        self.Position2=self.TIMER.counter()
        self.Delta=self.Position2-self.Position1
       
        if self.Delta > (AR+1)/2:
            self.Delta-=AR+1
        elif self.Delta < -(AR+1)/2:
            self.Delta+=AR+1
       
        self.EncPosition+=self.Delta
       
    def get_position(self):
        '''! @brief defines the get_position function
             @details this function will read the encoder positon 
             @param self
             @return self.EncPositon

        '''
       
        return self.EncPosition
   
    def zero(self):
        '''! @brief defines the zero function
             @details this function will zero the positon of the encoder
             @param self

        '''
        self.EncPosition = 0
        self.Position1=self.Position2
   
    def get_delta(self):
        '''! @brief defines the get_delta function
             @details this function will get the change in positon of based on
                      the encoder information
             @param self
             @return self.Delta

        '''
       
        return self.Delta
