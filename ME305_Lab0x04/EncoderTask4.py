"""!
@file EncoderTask4.py
@brief run a program that makes use of the information collected by other tasks
@details This code defines the state location and uses the information from encoder
         and time to update the motor location based on the encoder information 

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

from time import ticks_us, ticks_add, ticks_diff
import Shares
import encoder
import pyb
import math
import micropython
# import Shares, encoder, pyb, micropython, math and from time ticks_us, ticks_add,
# ticks_diff moduels for future use

## @brief defines State 0
#  @details uses micropython to assign the constant zero to this state
#
#
State0 = micropython.const(0)

## @brief defines State 0
#  @details uses micropython to assign the constant of one to this state
#
#
State1 = micropython.const (1)



def EncoderFunction(taskName, Period, zFlag, EncPosition, EncDelta, EncVel, EncVel2):
    '''! @brief creates a function called EcoderFunction
         @details this function collects the position and time of the encoder
                  and if the conditions are met chages state from 0 to 1
         @param taskName, Period, zFlag, EncPositon, EncDelta and EcVel
         @return Encoder position and time ellapse

    '''
    
    ## @brief defines state
    #  @details set the state to State 0
    #
    #
    state=State0

    ## @brief defines time_current
    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
    #
    #
    time_current=ticks_us()
    
    ## @brief defines time_future
    #  @details set time_future to the sum of the counter and the period
    #
    #
    time_future=ticks_add(time_current,Period)
    
    ## @brief defines Encoder1
    #  @details calls on the encoder file and Encoder class to set Encoder 1
    #
    #
    Encoder1=encoder.Encoder(4,pyb.Pin.AF2_TIM4,'PB6','PB7')
    
    while True:
        
        ## @brief defines time_passing
        #  @details set time_passing to ticks_us() an icreasing counter in miliseconds
        #
        #
        time_passing=ticks_us()
   
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,Period)
            if state==State0:
                Encoder1.update()
                EncDelta.write(Encoder1.get_delta())
                EncPosition.write(Encoder1.get_position())
                EncVel.write(EncDelta.read())
                EncVel2.write((EncDelta.read()/Period)*1000000*math.pi/4000)
                if zFlag.read():
                     state=State1
             
            elif state == State1:
               
                zFlag.write(False)
                Encoder1.zero()
                state = State0
               
            yield state
    else:
       yield None
