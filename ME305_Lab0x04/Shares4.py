"""!
@file Shares4.py
@brief creates the class Shares for future used.
@details This code creates the shares class so other programs can use it to read
         information or write infromation into a device. 

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-24-22
"""

class Share:
    '''!@brief      A standard shared variable.
        @details    Values can be accessed with read() or changed with write()
    '''
    def __init__(self, initial_value=None):
        '''!@brief      Constructs a shared variable
            @param      initial_value An optional initial value for the
                                      shared variable.
        '''
        self._buffer = initial_value
   
    def write(self, item):
        '''!@brief      Updates the value of the shared variable
            @param item The new value for the shared variable
        '''
        self._buffer = item
       
    def read(self):
        '''!@brief      Access the value of the shared variable
            @return    The value of the shared variable
        '''
        return self._buffer