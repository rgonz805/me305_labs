'''!@file                mainpage.py
    @brief               Brief doc for mainpage.py
    @details             Detailed doc for mainpage.py 

    @mainpage

    @section sec_intro   Introduction
                        This project focuses on creating a driver, motor, and 
                        encoder code to drive a motor with a desired speed, time, 
                        and direction. Also create a motor task to make use of 
                        the information of the previous files, and a User task 
                        to be able to provide input to manipulate moto behavior. 

    @section sec_mot     Motor Driver
                        Some information about the motor driver with links.
                        Please see motor.Motor for details.

    @section sec_enc     Encoder Driver
                        Some information about the encoder driver with links. 
                        Please see encoder.Encoder for details.

    @author              Erick Daza
    @author              Rodrigo Gonzalez

    
    @date                February 17, 2022
'''