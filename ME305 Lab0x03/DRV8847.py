"""!
@file DRV8847.py
@brief This code creates a driver class for the motor
@details This code defines the driver class, which includes the enable, disable
         fault and motor funtions to pass this information to the motor.

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""
from motor import Motor
from pyb import Pin, Timer, ExtInt
import time
# imports Motor class, Pin, Timer, time and ExInt for later use

class DRV8847:
    '''!@brief A driver class to use in the UserTask.
        @details Objects of this class can be used to set speed, directiong
                 and time of actuation to a given DC motor.

    '''
    def __init__(self):
        '''! @brief defines the _init_ function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self.timer, self.nsleep, self.faultInt

        '''
        self.timer = Timer(3, freq = 20_000)
        self.nsleep = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
        self.faultInt = ExtInt(Pin.cpu.B2, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=self.fault_cb)
       
    def enable(self):
        '''! @brief defines the enable function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self.faultInt.disable, self.nsleep, self.faultInt and time

        '''
        self.faultInt.disable()
        self.nsleep.high()
        time.sleep_us(50)
        self.faultInt.enable()
        pass
   
    def disable(self):
        '''! @brief defines the disable function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self.nsleep.low

        '''
        self.nsleep.low()
        pass
    def fault_cb(self, IRQ_src):
        '''! @brief defines the fault_cb function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self.disable
             @return "Fault Detected"

        '''
        self.disable()
        print("Fault Detected")
        pass
    def motor(self, IN1_pin, IN2_pin, ch1, ch2):
        '''! @brief defines the motor function
             @details this function will run an provide information using the
                      parameters listed below.
             @param self.timer, IN1_pin, IN2_pin, ch1 and ch2
             @return motor

        '''
        motor = Motor(self.timer, IN1_pin, IN2_pin, ch1, ch2)
        return motor
