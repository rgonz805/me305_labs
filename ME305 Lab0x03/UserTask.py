"""!
@file UserTask.py
@brief run a program that analyses user input and takes appropriate action
@details this  code analyses user input and transition between states if the 
         appropriate conditions have been met. 

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-017-22
"""

from pyb import USB_VCP
from time import ticks_us,ticks_add, ticks_diff
import micropython
import array
import math
# import USB_VCP, ticks_us, ticks_add, ticks_diff, micropython, array and math
# moduels for future use

## @brief defines State0
#  @details uses micropyton to give assign state a constant of zero
#
#
State0=micropython.const(0)

## @brief defines State1
#  @details uses micropyton to give assign state a constant of one
#
#
State1=micropython.const(1)

## @brief defines State2
#  @details uses micropyton to give assign state a constant of two
#
#
State2=micropython.const(2)

## @brief defines State3
#  @details uses micropyton to give assign state a constant of three
#
#
State3=micropython.const(3)

## @brief defines State4
#  @details uses micropyton to give assign state a constant of four
#
#
State4=micropython.const(4)

## @brief defines State5
#  @details uses micropyton to give assign state a constant of five
#
#
State5=micropython.const(5)

## @brief defines State6
#  @details uses micropyton to give assign state a constant of six
#
#
State6=micropython.const(6)

## @brief defines State7
#  @details uses micropyton to give assign state a constant of seven
#
#
State7=micropython.const(7)


def DuttySetter():
    '''!@brief Defines the DuttySetter function
        @details this function read user inputs and takes appropriate action
                 if the correct input is inputed then it gets added to the buffer
        @param User inputs
        @reture buf
    '''
    
    ## @brief creates a buffer object
    #  @details creates an empty object to add information later on
    #
    #
    buf=''
    
    ## @brief defines ser
    #  @details uses USB_VPC() to look for user inputs and store them in ser
    #
    #
    ser=USB_VCP()
    
    ## @brief creates FCN_Conditions
    #  @details set FCN_Conditions to True
    #
    #
    FCN_Condition=True
    while FCN_Condition==True:

        if ser.any():
            charIn=ser.read(1).decode()
           
            if charIn.isdigit():
                buf+=charIn
       
           
            elif charIn == '-':  
                if len(buf) == 0 :
                   buf+=charIn
       
           
            elif charIn in {'\b','\x08','\x7f'}  :
                buf=buf[:-1]
       
           
            elif charIn == '.':  
                if buf.count('.') == 0 :
                   buf+=charIn
       
            elif charIn in {'\r','\n'}:
                if len(buf) > 0 :
                    buf=round(float(buf),1)
                    if buf >=100:
                        buf=100
                    elif buf <=-100:
                        buf=-100  
                FCN_Condition=False
                return buf
   
def UserFunction(taskName,Period,zFlag,EncPosition,EncDelta,Duty1,Duty2,cFlag,EncVel):
    '''! @brief creates a function called UserFunction
         @details this function collects the position and time of the encoder
                  and if the conditions are met chages state from 0 to 7. Also
                  check for keyboard inputs.
         @param taskName, Period, zFlag, EncPositon, EncDelta
         @return current positon, delta value and zero the encoder
    
    ''' 
    
    ## @brief sets state to state 0
    #  @details sets the state to state 0
    #
    #
    state=State0
    
    ## @brief sets time_current 
    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
    #
    #
    time_current=ticks_us()
    
    ## @brief defines time_future
    #  @details set time_future to the sum of the counter and the period
    #
    #
    time_future=ticks_add(time_current,Period)
    
    ## @brief defines ser
    #  @details uses USB_VPC() to look for user inputs and store them in ser
    #
    #
    ser=USB_VCP()
    
    while True:
        time_passing=ticks_us()
        if ticks_diff(time_passing,time_future) >=0:
           
            if state == State0:
                state=State1
                printHelp()
               
            elif state == State1:

                if ser.any():
                    charIn=ser.read(1).decode()
                   
                    if charIn in {'z','Z'}:
                        zFlag.write(True)
                        print('Zeroing Encoder..')
                        state = State2
                       
                    elif charIn in {'p','P'}:
                        print(f'Encoder Position:{EncPosition.read()*2*math.pi/4000} Rads ')
                       
                    elif charIn in {'d','D'}:
                        print(f'Delta Value:{EncDelta.read()}')
                   
                    elif charIn in {'v','V'}:
                        print(f'Encoder 1 Velocity:{(EncVel.read()/Period)*2*1000000*math.pi/4000} Rad/Sec ')
                   
                    elif charIn in {'c','C'}:
                        cFlag.write(True)
                        print('Clearing Fault..')
                       
                    elif charIn in {'m'}:
                        print('Set the Duty Cycle for Motor1')
                        state = State5
                   
                    elif charIn in {'M'}:
                        print('Set the Duty Cycle for Motor2')
                        state = State6
                   
                    elif charIn in {'t','T'}:
                        print('Input motor1 Duty')
                        Dut1=DuttySetter()
                        Duty1.write(Dut1)
                        print('|Press t or T to change duty cycle speed          |')
                        print('|Press a or A to to collect data                  |')
                        print('|Press s or S to end Test and obtain data results |')
                        print('|Press c or C to clear fault                      |')
                        DataAvg=[]
                        state = State7
                   
                    elif charIn in {'g','G'}:
                        time_array= array.array('l',3001*[0])
                        position_array= array.array('l',3001*[0])
                        velocity_array = array.array('l',3001*[0])                      
                        idx=0
                        x=0
                        print('Initiating Data Collection For 30 Seconds')
                        state = State3
                    else:
                        print('Invalid Entry')

            elif state == State2:
                if not zFlag.read():
                    print('Encoder Position Zeroed')
                    state= State1
         
           
            elif state == State3:
                 if ticks_diff(time_passing,time_current) <= 30000000:
                             time_array[idx]=(ticks_diff(time_passing,time_current))
                             position_array[idx]=EncPosition.read()
                             velocity_array[idx]=EncVel.read()
                             idx+=1
                             if ser.any():
                                 charIn=ser.read(1).decode()
                                 
                                 if charIn == 's':
                                    print('Ending Data Collection Prematurely')
                                    state = State4
         
                 else:
                     state = State4
                     
            elif state== State4:
                     for i in range(len(position_array)):
                         if time_array[i] != 0:
                             x=i+1
                     t_array=array.array('l',x*[0])
                     p_array=array.array('l',x*[0])
                     v_array=array.array('l',x*[0])
                     for i in range(x):
                         t_array[i]=time_array[i]
                         p_array[i]=position_array[i]
                         v_array[i]=velocity_array[i]
                     for i in range(x):
                         print(f'Time : {round(t_array[i]/1_000_000,2)} Seconds , {p_array[i]*2*math.pi/4000} Rads, {(v_array[i]/Period)*2*1000000*math.pi/4000} Rad/Sec ')

                     state=State1
           
            elif state == State5:
                Dut1=DuttySetter()
                print(f'Motor 1 Duty Changed to {Dut1}')
                Duty1.write(Dut1)
                state = State1
           
            elif state == State6:
                Dut2=DuttySetter()
                print(f'Motor 2 Duty Changed to {Dut2}')
                Duty2.write(Dut2)
                state = State1
               
            elif state == State7:
                     if ser.any():
                         charIn=ser.read(1).decode()
                        
                         if charIn in {'a','A'}:
                             vel=[0]*100
                             for x in range(100):
                                 vel[x]=(EncVel.read()/Period)*2*1000000*math.pi/4000
                             avgvel=sum(vel)/100
                             print(f'At Duty {Dut1} speed is {avgvel}. Data values saved')
                             DataAvg.append([Dut1,avgvel])
                            
                        
                         elif charIn in {'c','C'}:
                             cFlag.write(True)
                             print('Clearing Fault..')
                        
                         elif charIn in {'t'}:
                             Dut1=0
                             Dut1=DuttySetter()
                             print(f'Motor 1 Duty Changed to {Dut1}')
                             Duty1.write(Dut1)

                         elif charIn in {'s','S'}:
                             print('Collected Data')
                             print('[Duty , Speed [Rad/Sec]]')
                             print(DataAvg)
                             state = State1
                         else:
                             print('Invalid Entry')
                   

                     
            yield state
        else:
               yield None

def printHelp():
    print('| Encoder Positioning Help Menu:                    |')
    print('|                                                   |')
    print('| Press z or Z to zero the encoder                  |')
    print('| Press p or P to obtain current position           |')
    print('| Press d or D to obtain the delta value            |')
    print('| Press v or V to obtain the velocity of Encoder 1  |')    
    print('| Press m to Enter a Duty Cycle for Motor 1         |')
    print('| Press M to Enter a Duty Cycle for Motor 2         |')
    print('| Press c or C to clear a fault condition           |')    
    print('| Press g or G to collect data for 30 seconds       |')
    print('| Press s or S to prematurely end data collection   |')
    print('| Press t or T to prematurely end data collection   |')