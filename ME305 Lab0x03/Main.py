"""!
@file Main.py
@brief run a program to regulate the speed and duty cycle of the motors
@details This code compiles all the information gathered by other files and 
         compiles and assigns it to task to control assure continuous gathering 
         and flow of information


@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

import Shares
import EncoderTask, UserTask, MotorTask
# import Shares, EncoderTask and UserTask moduels for future use

## @brief defines zFlag
#  @details uses the file Shares and Shares class to set zFlag to False
#
#
zFlag = Shares.Share(False)

## @brief defines EncPosition
#  @details uses the file Shares and Shares class to set EncPositon to zero
#
#
EncPosition = Shares.Share(0)

## @brief defines EncDelta
#  @details uses the file Shares and Shares class to set EncDelta to zero
#
#
EncDelta = Shares.Share(0)

## @brief defines Duty1
#  @details uses the file Shares and Shares class to set Duty1 to zero
#
#
Duty1 = Shares.Share(0)

## @brief defines Duty2
#  @details uses the file Shares and Shares class to set Duty2 to zero
#
#
Duty2 = Shares.Share(0)

## @brief defines EncVel
#  @details uses the file Shares and Shares class to set EncVel to zero
#
#
EncVel = Shares.Share(0)

## @brief defines cFlag
#  @details uses the file Shares and Shares class to set cFlag to False
#
#
cFlag = Shares.Share(False)


if __name__ == '__main__':
    
    ## @brief defines task1
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, EncVel, Duty1, Duty2, and cFlagas as
    #           parameters and stores the result in task1
    task1 = UserTask.UserFunction('Task User', 10_000, zFlag,EncPosition, EncDelta, Duty1, Duty2, cFlag, EncVel)
    
    ## @brief defines task2
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, and EncVel as parameters and stores the
    #           result in task2
    task2 = EncoderTask.EncoderFunction('Task Encoder', 10_000, zFlag, EncPosition, EncDelta, EncVel)
    
    ## @brief defines task3
    #  @details uses UserTask file and UserFunction with 10,000, cFlag, 
    #           Duty1, and Duty2 as parameters and stores the result in task3
    #           
    task3 = MotorTask.MotorFunction('Task Motor', 10_000 ,cFlag, Duty1, Duty2)
    
    ## @brief defines taskList
    #  @details puts task1, task2 and task3 into a list for later use
    #
    #
    taskList = {task1, task2, task3}
   
    while True:
        try:
        # an infinity loop that cycles between task1, task2 and task3
            for task in taskList:
                next(task1)
                next(task2)
                next(task3)
       
        except KeyboardInterrupt:
            break
           
print('Terminating Program')
