'''
    !@file       shares.py
    @brief      Task sharing ibrary implementing both shares and queues.
    @details    Implements a very simple interface for sharing data between
                multiple tasks.
'''

class Share:
    '''!@brief      A standard shared variable.
        @details    Values can be accessed with read() or changed with write()
    '''
    def __init__(self, initial_value=None):
        '''!@brief      Constructs a shared variable
            @param      initial_value An optional initial value for the
                                      shared variable.
        '''
        self._buffer = initial_value
   
    def write(self, item):
        '''!@brief      Updates the value of the shared variable
            @param item The new value for the shared variable
        '''
        self._buffer = item
       
    def read(self):
        '''!@brief      Access the value of the shared variable
            @return    The value of the shared variable
        '''
        return self._buffer