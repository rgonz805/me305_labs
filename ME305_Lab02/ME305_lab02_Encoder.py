"""!
@file ME305_lab02_Encoder.py
@brief run a program that makes use of the information collected by other tasks
@details This code defines zFlag, EncPosition and EncDelta and then uses those
         parameters and the data from other tasks to create a list for later use

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-03-22
"""

import pyb

class Encoder:
    def __init__(self,timer,pinnumber,channel1,channel2,):
         
        self.TIMER = pyb.Timer(timer, prescaler=0, period=65535)
        self.channel = self.TIMER.channel(1,pyb.Timer.ENC_AB)      
        self.pina =pyb.Pin(channel1, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnumber)
        self.pinb =pyb.Pin(channel2, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pinnumber)

        self.EncPosition = 0
        self.Position2 = 0
        self.Position1 = 0
        self.Delta = 0
       
    def update(self):
        AR = 65535
        self.Position1=self.Position2
        self.Position2=self.TIMER.counter()
        self.Delta=self.Position2-self.Position1
       
        if self.Delta > (AR+1)/2:
            self.Delta-=AR+1
        elif self.Delta < -(AR+1)/2:
            self.Delta+=AR+1
       
        self.EncPosition+=self.Delta
       
    def get_position(self):
       
        return self.EncPosition
   
    def zero(self):
          self.EncPosition = 0
          self.Position1=self.Position2
   
    def get_delta(self):
       
        return self.Delta