"""!
@file ME305_lab02_Main.py
@brief run a program in a loop for three different patterns for a LED
@details This code starts with the light not blinking, then it starts with a
         pattern of blinking with patters according to their states. The states 
         are state 1, state 2 and state 3. These states represent a pattern 
         corresponding to a sinewave , a square wave and a sawtooth wave 
         respectively. 


@author Erick Daza
@author Rodrigo Gonzalez
@date 02-03-22
"""
import Shares
import EncoderTask, UserTask
# import Shares, EncoderTask and UserTask moduels for future use

## @brief defines zFlag
#  @details uses Shares to set zFlag to False
#
#
zFlag = Shares.Share(False)

## @brief defines EncPosition
#  @details uses Shares to set EncPosition to zero
#
#
EncPosition = Shares.Share(0)

## @brief defines EncDelta
#  @details uses Shares to set EncDelta to zero
#
#
EncDelta = Shares.Share(0)



if __name__ == '__main__':
    
    ## @brief defines task1
    #  @details uses UserTask1 to create a function with 10,000, zFlag, EncPosition,
    #           and EncDelta as inputs
    #
    task1 = UserTask.UserFunction('Task User', 10_000, zFlag, EncPosition, EncDelta )
    
    ## @brief defines task2
    #  @details uses UserTask2 to create a function with 10,000, zFlag, EncPosition,
    #           and EncDelta as inputs
    #
    task2 = EncoderTask.EncoderFunction('Task Encoder', 10_000, zFlag, EncPosition, EncDelta)
    
    ## @brief defines taskList
    #  @details puts task1 and task2 into a list for later use
    #
    #
    taskList = {task1, task2}
   
    while True:
        try:
        #an infinite loop that runs until the condition is met
            for task in taskList:
                next(task1)
                next(task2)
       
        except KeyboardInterrupt:
            break
           
print('Terminating Program')