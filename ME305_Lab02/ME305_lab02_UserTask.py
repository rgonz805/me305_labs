"""!
@file ME305_lab02_UserTask.py
@brief run a program that makes use of the information collected by other tasks
@details This code defines zFlag, EncPosition and EncDelta and then uses those
         parameters and the data from other tasks to create a list for later use

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-03-22
"""
from pyb import USB_VCP
from time import ticks_us,ticks_add, ticks_diff
import micropython
import array
# import USB_VCP, ticks_us, ticks_add, and ticks_diff moduels for future use

## @brief defines State0
#  @details uses micropyton to give assign state a constant of zero
#
#
State0=micropython.const(0)

## @brief defines State1
#  @details uses micropyton to give assign state a constant of one
#
#
State1=micropython.const(1)

## @brief defines State2
#  @details uses micropyton to give assign state a constant of two
#
#
State2=micropython.const(2)

## @brief defines State3
#  @details uses micropyton to give assign state a constant of three
#
#
State3=micropython.const(3)

## @brief defines State4
#  @details uses micropyton to give assign state a constant of four
#
#
State4=micropython.const(4)

def UserFunction(taskName,Period,zFlag,EncPosition,EncDelta):
    '''! @brief creates a function called UserFunction
         @details this function collects the position and time of the encoder
                  and if the conditions are met chages state from 0 to 4. Also
                  check for keyboard inputs.
         @param taskName, Period, zFlag, EncPositon, EncDelta
         @return current positon, delta value and zero the encoder
    
    ''' 
    
    ## @brief sets state to state 0
    #  @details sets the state to state 0
    #
    #
    state=State0
    
    ## @brief sets time_current 
    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
    #
    #
    time_current=ticks_us()
    
    ## @brief defines time_future
    #  @details set time_future to the sum of the counter and the period
    #
    #
    time_future=ticks_add(time_current,Period)
    
    ## @brief defines ser
    #  @details set ser object to USB_VCP
    #
    #
    ser=USB_VCP()
    
    while True:
        time_passing=ticks_us()
        if ticks_diff(time_passing,time_future) >=0:
           
            if state == State0:
                state=State1
                printHelp()
               
            elif state == State1:
                if ser.any():
                    charIn=ser.read(1).decode()
                   
                    if charIn == 'z':
                        zFlag.write(True)
                        print('Zeroing Encoder..')
                        state = State2
                       
                    elif charIn == 'p':
                        print(f'Encoder Position:{EncPosition.read()}')
                       
                    elif charIn == 'd':
                        print(f'Delta Value:{EncDelta.read()}')
                    elif charIn == 'g':
                        time_array= array.array('l',3001*[0])
                        position_array= array.array('l',3001*[0])                        
                        idx=0
                        x=0
                        print('Initiating Data Collection For 30 Seconds')
                        state = State3
                    else:
                        print('Invalid Entry')

            elif state == State2:
                if not zFlag.read():
                    print('Encoder Position Zeroed')
                    state= State1
         
           
            elif state == State3:
                 if ticks_diff(time_passing,time_current) <= 30000000:
                             time_array[idx]=(ticks_diff(time_passing,time_current))
                             position_array[idx]=EncPosition.read()
                             idx+=1
                             if ser.any():
                                 charIn=ser.read(1).decode()
                                 
                                 if charIn == 's':
                                    print('Ending Data Collection Prematurely')
                                    state = State4
         
                 else:
                     state = State4
                     
            elif state== State4:
                     for i in range(len(position_array)):
                         if time_array[i] != 0:
                             x=i+1
                     t_array=array.array('l',x*[0])
                     p_array=array.array('l',x*[0])
                     for i in range(x):
                         t_array[i]=time_array[i]
                         p_array[i]=position_array[i]
                     for i in range(x):
                         print(f'Time : {round(t_array[i]/1_000_000,2)} Seconds , {p_array[i]} Ticks')

                     state=State1
            yield state
        else:
               yield None

def printHelp():
    print('| Encoder Positioning Help Menu:               |')
    print('|                                              |')
    print('| Note: The commands below are case sensitive  |')
    print('| Enter lower case letters only                |')
    print('|                                              |')
    print('| Press g to collect data for 30 seconds       |')
    print('| Press p to obtain current position           |')
    print('| Press d to obtain the delta value            |')
    print('| Press z to zero the encoder                  |')
    print('| Press s to prematurely end data collection   |')