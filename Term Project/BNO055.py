# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 18:28:18 2022

@author: melab15
"""

import pyb
import struct
import time

class BNO055:
    
    def __init__ (self,bus):
        self.bus=bus
        self.I2C = pyb.I2C(self.bus,pyb.I2C.CONTROLLER)
        self.I2C.mem_write(0b00000000, 0x28, 0x3D)
        self.buf = bytearray(22*[0])
        time.sleep_ms(50)
        pass
    
    def altMode(self):
        self.I2C.mem_write(0b00000000, 0x28, 0x3D)
        pass
    
    def imuMode(self):
        self.I2C.mem_write(0b00001000, 0x28, 0x3D)
        pass
    
    def calMode(self):
        calStat= self.I2C.mem_read(1,0x28,0x35)[0]
        self.magStat = calStat & 0b00000011
        self.accStat = (calStat & 0b00001100) >> 2
        self.gyrStat = (calStat & 0b00110000) >> 4
        self.sysStat = (calStat & 0b11000000) >> 6
        
        return (self.magStat,self.accStat,self.gyrStat,self.sysStat)
    
    
    def euler_Ang(self):
        data=self.I2C.mem_read(0b0110,0x28,0x1A)
        (head,roll,pitch) = struct.unpack('<hhh' , data)
        return(head/16,roll/16,pitch/16)
    
    def Angular_Vel(self):
        data=self.I2C.mem_read(0b0110,0x28,0x14)
        (headvel,rollvel,pitchvel) = struct.unpack('<hhh' , data)
        return(headvel/16,rollvel/16,pitchvel/16)
    
    def get_calcoef(self):
        self.I2C.mem_read(self.cal_buf,0x28,0x55)
        return self.cal_buf
    
    def set_calcoef(self,val):
        self.I2C.mem_write(val,0x28,0x55)
        pass
        
if __name__=="__main__":

    IMU = BNO055(1)
    IMU.altMode()
    
    while True:
        print(IMU.calMode())