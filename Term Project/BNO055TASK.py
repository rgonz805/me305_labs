# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 12:48:02 2022

@author: melab15
"""
import time
from time import ticks_us,ticks_add, ticks_diff
import BNO055, os
import micropython
import pyb


#def BNO055Function(taskName, Period, pFlag, Position, vel, calStatReturn, CalibrationFlag, EnaFlag, calDATA, cFlag):
def BNO055Function(taskName, Period, pFlag, Position,CalibrationFlag, EnaFlag,cFlag,vFlag,Velocity, CalMode):
    '''!@brief          Serves as a medium between user_task.py and encoder.py
        @details        Facillitates communication between user_task.py and the encoder driver,
                        encoder.py. This code will recieve signals from user_task and go and get
                        information from the encoder driver.
                        
        @param state    keeps track of the state of the system
        @param colArray
    
    '''

    ##  @brief      Keeps track of which state the encoder task is in
    #   
    State0=micropython.const(0)
    State1=micropython.const(1)
    State2=micropython.const(2)
    
    filename = "IMU_cal_coeffs.txt"
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , Period)
    
    I2C = BNO055.BNO055(1)
    time_current=ticks_us()
    
    ## @ brief defines time_future
    #  @ details uses ticks_us to assing a time in milliseconds by adding time 
    #            ellapse to current time
    #
    time_future=ticks_add(time_current,Period)
    
    state = State0

    while True:
        time_passing=ticks_us()
#        Position.write(I2C.euler_Ang())
#        CalMode.write(I2C.calMode())
#        Velocity.write(I2C.Angular_Vel())
        
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,Period)      
                
            if state == State0:
                    if filename in os.listdir():
                        state = State2
                        print('Calibration File Found')
                    else:
                        state = State1
                    
            if state == State1:    
                    Position.write(I2C.euler_Ang())                    
                    Velocity.write(I2C.Angular_Vel())
                    
                    if CalibrationFlag.read(): 
                        
                        CalMode.write(I2C.calMode())
                        

                    elif pFlag.read():
                        Position.write(I2C.euler_Ang())
                        pFlag.write(False)
                    
                    elif vFlag.read():
                        Velocity.write(I2C.Angular_Vel())
                        vFlag.write(False)
                    
                        
                    elif EnaFlag.read():
                        EnaFlag.write(False)
                        I2C.altMode()
                        I2C.imuMode()
                        Position.write(I2C.euler_Ang())
                        
            elif state == State2:
                    I2C.altMode()                  
                    I2C.imuMode()
#                    if filename in os.listdir():
                    with open(filename, 'r') as CalibFile:
                        CalCoefHex = CalibFile.readline()
                    CalCoefArray = CalCoefHex.split(',')
                    for x in range(len(CalCoefArray)):
                        CalCoefArray[x] = int(CalCoefArray[x], 16)
                        I2C.buf[x]=CalCoefArray[x]
                    I2C.set_calcoef(I2C.buf)
                    cFlag.write(True)
                    state = State1
#                            my_i2c.bufcal[i] = calCoeffs[i]
#                        my_i2c.configMode()
#                        my_i2c.calCoefWrite(my_i2c.bufcal)
#                        my_i2c.imuMode()
#                        #flag
#                        state = 1
#                    else:
#                        my_i2c.imuMode
#                        my_i2c.calStat()
#                        calDATA.write((my_i2c.mag_stat, my_i2c.acc_stat, my_i2c.gyr_stat, my_i2c.sys_stat))
#                        yield calDATA
#                        if my_i2c.mag_stat == 0 and my_i2c.acc_stat == 3 and my_i2c.gyr_stat == 3 and my_i2c.sys_stat == 0:
#                            my_i2c.calCoefRead()
#                            calCoeffs = my_i2c.calCoefRead()
#                            strList = []
#                            for calCoeff in calCoeffs:
#                                strList.append(hex(calCoeff))
#                            with open(filename, 'w') as f:
#                                f.write(','.join(strList))
#                                state = 1
#                            
                            
                        
                    
            else:
                raise ValueError(f"invalid state in {taskName}")
                
                next_time = time.ticks_add(next_time , Period)
                
                yield state
                
        else:
            yield None