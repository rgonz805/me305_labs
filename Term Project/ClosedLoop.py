# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 16:32:11 2022

@author: melab15
"""

class ClosedLoop:
    def __init__(self,ref):
         
        self.ref=ref

    def set_Kp(self,kp):
        
        self.Kp=kp

    def set_Kd(self,kd):
        
        self.Kd=kd
    

    def Control(self,position,velocity):
        self.position=position
        self.velocity=velocity
        self.control = -self.Kp*(self.ref-self.position) + self.Kd*(self.velocity)
        
        return self.control
