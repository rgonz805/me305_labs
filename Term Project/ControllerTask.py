# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 17:21:01 2022

@author: melab15
"""

from time import ticks_us, ticks_add, ticks_diff
import Shares
import ClosedLoop
import micropython
# import Shares, encoder, pyb, micropython, math and from time ticks_us, ticks_add,
# ticks_diff moduels for future use

## @brief defines State 0
#  @details uses micropython to assign the constant zero to this state
#
#
State0 = micropython.const(0)

## @brief defines State 0
#  @details uses micropython to assign the constant of one to this state
#
#
#State1 = micropython.const (1)



def ControllerFunction(taskName, Period, gain, ref_vel, EncVel2, wFlag, Duty1, ActLev):
    
    ## @brief defines state
    #  @details set the state to State 0
    #
    #
    state=State0

    ## @brief defines time_current
    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
    #
    #
    time_current=ticks_us()
    
    ## @brief defines time_future
    #  @details set time_future to the sum of the counter and the period
    #
    #
    time_future=ticks_add(time_current,Period)
    
    ## @brief defines Encoder1
    #  @details calls on the encoder file and Encoder class to set Encoder 1
    #
    #
    Controller=ClosedLoop.ClosedLoop(0, 0)
    
    while True:
        
        ## @brief defines time_passing
        #  @details set time_passing to ticks_us() an icreasing counter in miliseconds
        #
        #
        time_passing=ticks_us()
   
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,Period)
            if state==State0:
               if wFlag.read():
                   Controller.set_ref_vel(ref_vel.read())
                   Controller.set_Gain(gain.read())
                   Controller.error_calc(EncVel2.read())
                   duty=Controller.Actuation(Duty1.read())
                   Duty1.write(duty)
                   ActLev.write(duty)

            else:
                pass
            
            time_future=ticks_add(time_future,Period)
            
            yield state
    else:
       yield None
