# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 00:21:11 2022

@author: melab15
"""
import micropython
from time import ticks_us,ticks_add, ticks_diff
import tPAD
from pyb import Pin
from pyb import Pin
from time import ticks_us,ticks_add, ticks_diff
import Shares
import motor
import ClosedLoop
import micropython
# imports Pin, ticks_us, ticks_add, ticks_diff, DRV8847, Shares, and micropython for later use

## @brief defines State0
#  @details uses micropython to assing the constant 0 to State0
#
#
State0 = micropython.const(0)

## @brief defines State1
#  @details uses micropython to assing the constant 1 to State1
#
#
State1 = micropython.const(1)

## @brief defines motor_drv
#  @details uses DRV8847 to store its information into this variable
#
#

def MotorFunction(taskName):
    '''! @brief defines the _init_ function
         @details this function will run an provide information using the
                  parameters listed below.
         @param self.timer, self.nsleep, self.faultInt

    '''
    
    ## @brief defines state
    #  @details set state to State0
    #
    #
    state=State0

    ## @brief defines motor_1
    #  @details uses motor_drv to assingned motor1 the correct pins
    #
    #
    motor_1 = motor.Motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    
    ## @brief defines motor_2
    #  @details uses motor_drv to assingned motor2 the correct pins
    #
    #
    motor_2 = motor.Motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
       

    
    ## @ brief defines time_current 
    #  @ details uses ticks_us to assing a time in milliseconds
    #
    #
    time_current=ticks_us()
    
    ## @ brief defines time_future
    #  @ details uses ticks_us to assing a time in milliseconds by adding time 
    #            ellapse to current time
    #
    time_future=ticks_add(time_current,10_000)

    while True:
        time_passing=ticks_us()
   
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,10_000)
##import array
##import math
## import USB_VCP, ticks_us, ticks_add, ticks_diff, micropython, array and math
## moduels for future use
#
### @brief defines State0
##  @details uses micropyton to give assign state a constant of zero
##
##
#State0=micropython.const(0)
#
#
#def TouchPADFunction(taskName):
#    
#    '''! @brief creates a function called UserFunction
#         @details this function collects the position and time of the encoder
#                  and if the conditions are met chages state from 0 to 7. Also
#                  check for keyboard inputs.
#         @param taskName, Period, zFlag, EncPositon, EncDelta
#         @return current positon, delta value and zero the encoder
#    
#    ''' 
#    
#    ## @brief sets state to state 0
#    #  @details sets the state to state 0
#    #
#    #
#    state=State0
#    
#    ## @brief sets time_current 
#    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
#    #
#    #
#    time_current=ticks_us()
#    
#    ## @brief defines time_future
#    #  @details set time_future to the sum of the counter and the period
#    #
#    #
#    time_future=ticks_add(time_current,10_000)
#    
#    ## @brief defines ser
#    #  @details uses USB_VPC() to look for user inputs and store them in ser
#    #
#    #
##    x=tPAD.TouchPad(Pin.cpu.A7,Pin.cpu.A1,Pin.cpu.A6,Pin.cpu.A0,176,100)
#    
#    while True:
#        time_passing=ticks_us()
#        if ticks_diff(time_passing,time_future) >=0:
#           
#            if state == State0:
#                print('cool')
#                
##                print(f'X:{Ball_x.read()}    Y:{Ball_y.read()}')