"""!
@file motor.py
@brief run a program that makes use of the information collected by other tasks
@details This code defines the motor class for use in DRV8847

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""
import pyb
from pyb import Pin, Timer
# imports pyb and from pyb imports Pin and Timer

class Motor:
   
    '''!@brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
                 DC motor.

    '''
    def __init__ (self, IN1_pin, IN2_pin, A, B):
       
        '''!@brief Initializes and returns an object associated with a DC Motor.
            @details Objects of this class should not be instantiated
                     directly. Instead create a DRV8847 object and use
                     that to create Motor objects using the method
                     DRV8847.motor().
        '''
        
        ## @brief defines self.PWM
        #  @details uses self.PWM to set the time
        #
        #
        self.PWM_tim= pyb.Timer(3, freq = 20_000)
        
        ## @brief defines self.Pin1
        #  @details assingns pin1 to IN1
        #
        #
        self.Pin1 = IN1_pin
        
        ## @brief defines self.Pin2
        #  @details assingns pin1 to IN2
        #
        #
        self.Pin2 = IN2_pin
       
        ## @brief defines self.T3_CH_A
        #  @details stores the result of self.PWM with sertain parameters into
        #           T3_CH_A
        # 
        self.T3_CH_A= self.PWM_tim.channel(A, pyb.Timer.PWM_INVERTED, pin = self.Pin1)
        
        ## @brief defines self.T3_CH_B
        #  @details stores the result of self.PWM with sertain parameters into
        #           T3_CH_B
        # 
        self.T3_CH_B= self.PWM_tim.channel(B, pyb.Timer.PWM_INVERTED, pin = self.Pin2)
        pass
       
       
           
   
    def set_duty (self, duty):        
        '''!@brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent
                     to the motor to the given level. Positive values
                     cause effort in one direction, negative values
                     in the opposite direction.
            @param duty A signed number holding the duty
                        27 cycle of the PWM signal sent to the motor
        '''
        if duty >= 0:
            self.T3_CH_A.pulse_width_percent(0)
            self.T3_CH_B.pulse_width_percent(duty)

        elif duty < 0:
            self.T3_CH_A.pulse_width_percent(-duty)
            self.T3_CH_B.pulse_width_percent(0)
        pass
