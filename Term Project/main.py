"""!
@file Main.py
@brief run a program to regulate the speed and duty cycle of the motors
@details This code compiles all the information gathered by other files and 
         compiles and assigns it to task to control assure continuous gathering 
         and flow of information


@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

import Shares
import BNO055TASK, UserTask, MotorTask, TouchPAD
# import Shares, EncoderTask and UserTask moduels for future use

## @brief defines zFlag
#  @details uses the file Shares and Shares class to set zFlag to False
#
#
pFlag = Shares.Share(False)

## @brief defines EncPosition
#  @details uses the file Shares and Shares class to set EncPositon to zero
#
#
Position = Shares.Share(0)

## @brief defines EncDelta
#  @details uses the file Shares and Shares class to set EncDelta to zero
#
#
CalibrationFlag = Shares.Share(False)

## @brief defines Duty1
#  @details uses the file Shares and Shares class to set Duty1 to zero
#
#
Kp= Shares.Share(0)

## @brief defines Duty2
#  @details uses the file Shares and Shares class to set Duty2 to zero
#
#
Kd= Shares.Share(0)

## @brief defines EncVel
#  @details uses the file Shares and Shares class to set EncVel to zero
#
#

Velocity = Shares.Share(0)

rp = Shares.Share(0)

rr = Shares.Share(0)

## @brief defines cFlag
#  @details uses the file Shares and Shares class to set cFlag to False
#
#
cFlag = Shares.Share(False)

wFlag = Shares.Share(False)

ActLev = Shares.Share(0)

EnaFlag = Shares.Share(False)

vFlag = Shares.Share(False)

CalMode=Shares.Share(0)

Ball_x=Shares.Share(0)

Ball_y=Shares.Share(0)

Dutyx = Shares.Share(0)

Caliball = Shares.Share(False)

if __name__ == '__main__':
    
    ## @brief defines task1
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, EncVel, Duty1, Duty2, and cFlagas as
    #           parameters and stores the result in task1
    task1 = UserTask.UserFunction('Task User', 10_000, pFlag,Position, CalibrationFlag, Kp, Kd, cFlag, Velocity ,wFlag, rr, rp, ActLev,EnaFlag ,vFlag , CalMode, Ball_x , Ball_y, Dutyx, Caliball)
    
    ## @brief defines task2
    #  @details uses UserTask file and UserFunction with 10,000, zFlag, 
    #           EncPosition, EncDelta, and EncVel as parameters and stores the
    #           result in task2
#    task2 = EncoderTask.EncoderFunction('Task Encoder', 10_000, zFlag, EncPosition, EncDelta, EncVel, EncVel2)
    task2 = BNO055TASK.BNO055Function('Task BNO055', 10_000,pFlag, Position,CalibrationFlag, EnaFlag,cFlag,vFlag,Velocity, CalMode)
    ## @brief defines task3
    #  @details uses UserTask file and UserFunction with 10,000, cFlag, 
    #           Duty1, and Duty2 as parameters and stores the result in task3
    #           
    task3 = MotorTask.MotorFunction('Task Motor', 10_000 ,wFlag, Kp, Kd, rr, rp, Position, Velocity, Ball_x , Ball_y, Dutyx, Caliball)
    
#    task4 = TouchPAD.TouchPADFunction('Task Touch Pad', 10_000, Ball_x , Ball_y)
    
    ## @brief defines taskList
    #  @details puts task1, task2 and task3 into a list for later use
    #
    #
#    task5=TouchPAD.MotorFunction('asas')
    
#    taskList = {task1, task2, task3, task4}
    taskList = {task1, task2, task3}
   
    while True:
        try:
        # an infinity loop that cycles between task1, task2 and task3
            for task in taskList:
                next(task1)
                next(task2)
                next(task3)
#                next(task4)
       
        except KeyboardInterrupt:
            break
           
print('Terminating Program')
