"""!
@file UserTask.py
@brief run a program that analyses user input and takes appropriate action
@details this  code analyses user input and transition between states if the 
         appropriate conditions have been met. 

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-017-22
"""

from pyb import USB_VCP
from time import ticks_us,ticks_add, ticks_diff
import micropython
import tPAD
import array
#import math
# import USB_VCP, ticks_us, ticks_add, ticks_diff, micropython, array and math
# moduels for future use

## @brief defines State0
#  @details uses micropyton to give assign state a constant of zero
#
#
State0=micropython.const(0)

## @brief defines State1
#  @details uses micropyton to give assign state a constant of one
#
#
State1=micropython.const(1)

## @brief defines State2
#  @details uses micropyton to give assign state a constant of two
#
#
State2=micropython.const(2)

## @brief defines State3
#  @details uses micropyton to give assign state a constant of three
#
#
State3=micropython.const(3)

## @brief defines State4
#  @details uses micropyton to give assign state a constant of four
#

def DuttySetter():
    '''!@brief Defines the DuttySetter function
        @details this function read user inputs and takes appropriate action
                 if the correct input is inputed then it gets added to the buffer
        @param User inputs
        @reture buf
    '''
    
    ## @brief creates a buffer object
    #  @details creates an empty object to add information later on
    #
    #
    buf=''
    
    ## @brief defines ser
    #  @details uses USB_VPC() to look for user inputs and store them in ser
    #
    #
    ser=USB_VCP()
    
    ## @brief creates FCN_Conditions
    #  @details set FCN_Conditions to True
    #
    #
    FCN_Condition=True
    while FCN_Condition==True:

        if ser.any():
            charIn=ser.read(1).decode()
           
            if charIn.isdigit():
                buf+=charIn
       
           
            elif charIn == '-':  
                if len(buf) == 0 :
                   buf+=charIn
       
           
            elif charIn in {'\b','\x08','\x7f'}  :
                buf=buf[:-1]
       
           
            elif charIn == '.':  
                if buf.count('.') == 0 :
                   buf+=charIn
       
            elif charIn in {'\r','\n'}:
                if len(buf) > 0 :
                    buf=round(float(buf),1)
                    if buf >=100:
                        buf=100
                    elif buf <=-100:
                        buf=-100  
                FCN_Condition=False
                return buf

def Gen_Num_Set():
    '''!@brief Defines the Gen_Num_Set function
        @details this function is similar to the dutty setter function except
                 that it is for setting numbers beyond the range of -100<x<100
        @param User inputs
        @reture buf
    '''
    
    ## @brief creates a buffer object
    #  @details creates an empty object to add information later on
    #
    #
    buf=''
    
    ## @brief defines ser
    #  @details uses USB_VPC() to look for user inputs and store them in ser
    #
    #
    ser=USB_VCP()
    
    ## @brief creates FCN_Conditions
    #  @details set FCN_Conditions to True
    #
    #
    FCN_Condition=True
    while FCN_Condition==True:

        if ser.any():
            charIn=ser.read(1).decode()
           
            if charIn.isdigit():
                buf+=charIn
       
           
            elif charIn == '-':  
                if len(buf) == 0 :
                   buf+=charIn
       
           
            elif charIn in {'\b','\x08','\x7f'}  :
                buf=buf[:-1]
       
           
            elif charIn == '.':  
                if buf.count('.') == 0 :
                   buf+=charIn
       
            elif charIn in {'\r','\n'}:
                if len(buf) > 0 :
                    buf=round(float(buf),1)
                FCN_Condition=False
                return buf
   
def UserFunction(taskName, Period, pFlag,Position, CalibrationFlag, Kp, Kd, cFlag, Velocity ,wFlag, rr, rp, ActLev,EnaFlag ,vFlag, CalMode , Ball_x , Ball_y, Dutyx, Caliball):
    
    '''! @brief creates a function called UserFunction
         @details this function collects the position and time of the encoder
                  and if the conditions are met chages state from 0 to 7. Also
                  check for keyboard inputs.
         @param taskName, Period, zFlag, EncPositon, EncDelta
         @return current positon, delta value and zero the encoder
    
    ''' 
    
    ## @brief sets state to state 0
    #  @details sets the state to state 0
    #
    #
    state=State0
    
    ## @brief sets time_current 
    #  @details set time_current to ticks_us() an icreasing counter in miliseconds
    #
    #
    time_current=ticks_us()
    
    ## @brief defines time_future
    #  @details set time_future to the sum of the counter and the period
    #
    #
    time_future=ticks_add(time_current,Period)
    
    ## @brief defines ser
    #  @details uses USB_VPC() to look for user inputs and store them in ser
    #
    #
    px_array = array.array('l',1500*[0])
    py_array = array.array('l',1500*[0])
    t_array = array.array('l',1500*[0])
    roll_array = array.array('l',1500*[0])
    pitch_array = array.array('l',1500*[0])
    i=0
    ser=USB_VCP()
    while True:
        time_passing=ticks_us()
        if ticks_diff(time_passing,time_future) >=0:
           
            if state == State0:
                state=State1
                printHelp()
               
            elif state == State1:

                if ser.any():
                    charIn=ser.read(1).decode()
                   
                    if charIn in {'p', 'P'}:
                        pFlag.write(True)
                        head,roll,pitch=Position.read()
                        print(f'Head:{head},Roll:{roll},Pitch:{pitch}')
                    
                    elif charIn in {'b', 'B'}:
                        print(f'X Ball Position:{Ball_x.read()}')
                        print(f'Y Ball Position:{Ball_y.read()}')

                       
                    elif charIn in {'v', 'V'}:
                        vFlag.write(True)
                        head,roll,pitch=Velocity.read()
                        print(f'Head:{head}, Roll:{roll}, Pitch:{pitch}')
                       
                    elif charIn in {'e','E'}:
                        EnaFlag.write(True)
                        print('Enabling BNO055')
                    
                    elif charIn in {'c','C'}:
                        if cFlag.read() == True:
                            print('Calibration File Was Found')
                            print('No Further Calibration is Necessary')
                        else:
                            print('Begin Calibration')
                            CalibrationFlag.write(True)
                            state = State2
                    
                    elif charIn in {'x','x'}:
                            print('Begin Calibration')
                            Caliball.write(True)

                            
                    elif charIn in {'w','W'}:
                        if not wFlag.read():
                            print('Contol Active')
                            wFlag.write(True)
                        else:
                            print('No Active Control')
                            wFlag.write(False)
                            
                    elif charIn in {'k','K'}:
                        print('Enter values for Kp')
                        kp = Gen_Num_Set()
                        Kp.write(kp)
                        print('Enter values for Kd')  
                        kd = Gen_Num_Set()
                        Kd.write(kd)
                        print(f'Kp={kp}, Kd={kd}')
                   
                    elif charIn in {'r', 'R'}:
                        print('Enter Reference Roll Value')
                        ref_roll = Gen_Num_Set()
                        rr.write(ref_roll)
                        print(f'Roll Value = {rr.read()}')
                        
                        print('Enter Reference Pitch Value')
                        ref_pitch = Gen_Num_Set()
                        rp.write(ref_pitch)
                        print(f'Pitch Value = {rp.read()}')
                    
                    elif charIn in {'g', 'G'}:
                        i=0
                        state = State3
                
                
            elif state == State2:
#                (magStat, accStat, gyroStat, sysStat) = CalMode.read()
                print(CalMode.read())
#                if accStat == 3 and gyroStat == 3:
#                    print('Done Calibrating')
#                    state = State1
#                x = CalMode.read()
#                print(x)
                if ser.any():
                    charIn=ser.read(1).decode()
                    
                    if charIn in {'c','C'}:
                         CalibrationFlag.write(False)
                         print('Done Calibrating')
                         state = State0
            
            elif state == State3:
                i+=1
                head,roll,pitch=Position.read()
                t_array[i] = ticks_diff(time_passing,time_future)
                roll_array[i] = int(roll)
                pitch_array[i] = int(pitch)
                px_array[i]=int(Ball_x.read())
                py_array[i] = int(Ball_y.read())
                if i==1499:
                    print(t_array)
                    print(roll_array)
                    print(pitch_array)
                    print(px_array)
                    print(py_array)
                    state = State1

            yield state
        else:
               yield None

def printHelp():
    print('|                     Help Menu:                     |')
    print('|                                                    |')
    print('| Press p or P to obtain current position            |')
    print('| Press v or V to obtain the velocity                |')    
    print('| Press e or E to enable BNO055                      |')
    print('| Press k or K to set a gain value for the controller|')
    print('| Press w or W to enable the controller              |')
    print('| Press r or R set reference values                  |')
    print('| Press c or C to calibrate platform                 |')
    print('| Press b or B to to get the position of the ball    |')
    print('| Press g or G to collect data                       |')
    print('| Press x or X to calibrate touchpad                 |')