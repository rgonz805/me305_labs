"""!
@file MotorTask.py
@brief run a program that uses the information DRV8847 and Shares
@details This code uses the information from the driver, shares, cFlags to 
         run the motor with the appropriate speed or stop the motor when a fault 
         is detected

@author Erick Daza
@author Rodrigo Gonzalez
@date 02-17-22
"""

from pyb import Pin
from time import ticks_us,ticks_add, ticks_diff
import Shares
import motor
import ClosedLoop
import micropython
import tPAD
# imports Pin, ticks_us, ticks_add, ticks_diff, DRV8847, Shares, and micropython for later use

## @brief defines State0
#  @details uses micropython to assing the constant 0 to State0
#
#
State0 = micropython.const(0)

## @brief defines State1
#  @details uses micropython to assing the constant 1 to State1
#
#
State1 = micropython.const(1)

## @brief defines motor_drv
#  @details uses DRV8847 to store its information into this variable
#
#

def MotorFunction(taskName,Period, wFlag, Kp, Kd, rr, rp, Position, Velocity, Ball_x , Ball_y , Dutyx, Caliball):
    '''! @brief defines the _init_ function
         @details this function will run an provide information using the
                  parameters listed below.
         @param self.timer, self.nsleep, self.faultInt

    '''
    
    ## @brief defines state
    #  @details set state to State0
    #
    #
    state=State0

    ## @brief defines motor_1
    #  @details uses motor_drv to assingned motor1 the correct pins
    #
    #
    motor_1 = motor.Motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    
    ## @brief defines motor_2
    #  @details uses motor_drv to assingned motor2 the correct pins
    #
    #
    motor_2 = motor.Motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
       

    
    ## @ brief defines time_current 
    #  @ details uses ticks_us to assing a time in milliseconds
    #
    #
    time_current=ticks_us()
    
    ## @ brief defines time_future
    #  @ details uses ticks_us to assing a time in milliseconds by adding time 
    #            ellapse to current time
    #
    time_future=ticks_add(time_current,Period)
    x=tPAD.TouchPad(Pin.cpu.A7,Pin.cpu.A1,Pin.cpu.A6,Pin.cpu.A0,176,100)
    while True:
        time_passing=ticks_us()
   
        if ticks_diff(time_passing,time_future) >=0:
            time_future=ticks_add(time_future,Period)
            xbp, ybp, z =x.XYZ_Scan()
            vx, vy =x.Speed(x.XYZ_Scan())
#            print(vx,vy)
            if z != 1:
                xbp=0
                ybp=0
#            print(xbp,ybp)
            Ball_x.write(xbp)
            Ball_y.write(ybp)
            if Caliball.read():
                x.Calibrate()
            if wFlag.read():
                xbp, ybp, z =x.XYZ_Scan()
#                vx, vy =x.Speed(x.XYZ_Scan())
#                print(vx,vy)
                if z != 1:
                    xbp=0
                    ybp=0
#                print(xbp,ybp)
                Ball_x.write(xbp)
                Ball_y.write(ybp)
                
                Controller_roll=ClosedLoop.ClosedLoop(rr.read())
                Controller_roll.set_Kp(Kp.read())
                Controller_roll.set_Kd(Kd.read())
                
                Controller_pitch=ClosedLoop.ClosedLoop(rp.read())
                Controller_pitch.set_Kp(Kp.read())
                Controller_pitch.set_Kd(Kd.read())
                
#                head,roll,pitch=Position.read()
                headvel,rollvel,pitchvel=Velocity.read()
                
                motor_1.set_duty(Controller_pitch.Control(xbp,pitchvel))
#                Dutyx.write(Controller_pitch.Control(xbp,-pitchvel)) 
                
                motor_2.set_duty(Controller_pitch.Control(-ybp,rollvel))

            

            else:
                motor_1.set_duty(0)
                motor_2.set_duty(0)
        else:
            yield None