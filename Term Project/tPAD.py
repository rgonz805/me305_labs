# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 19:35:06 2022

@author: melab15
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 18:19:56 2022

@author: melab15
"""

from pyb import Pin, ADC
import os
import utime
from ulab import numpy

class TouchPad:
    def __init__ (self,X_p,X_m,Y_p,Y_m, Width, Length):
        '''!@brief 

        '''
        #m = minus
        #p = plus
        self.X_p = Pin(X_p)
        self.X_m = Pin(X_m)
        self.Y_p = Pin(Y_p)
        self.Y_m = Pin(Y_m)
        
        self.Width=Width
        self.Length=Length
        
        self.K_xx = 1
        self.K_yx = 1
        self.K_xy = 1
        self.K_yy = 1
        self.x0 = 1
        self.y0 = 1
        
        pass
    
    def XScan (self):
        '''!@brief 
        '''
        self.X_p = Pin(Pin.cpu.A7, Pin.OUT_PP)
        self.X_m = Pin(Pin.cpu.A1, Pin.OUT_PP)
        
        self.X_m.low()
        self.X_p.high()
        
        self.Y_p = Pin(Pin.cpu.A6, Pin.IN)
        self.Y_m = ADC(Pin(Pin.cpu.A0))
        

        X_ADC = self.Y_m
        Y_m = X_ADC.read()
        
        X = ((Y_m/4096)*self.Width)-self.Width/2
        self.x = self.K_xx*X + self.x0
        
        return self.x
    
    def YScan (self):
        '''!@brief 
        '''
        self.Y_p = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.Y_m = Pin(Pin.cpu.A0, Pin.OUT_PP)
        
        self.Y_p.high()
        self.Y_m.low()
        
        self.X_p = Pin(Pin.cpu.A7, Pin.IN)
        self.X_m = ADC(Pin(Pin.cpu.A1))
        
        X_ADC = self.X_m
        X_m = X_ADC.read()
        
        Y = ((X_m/4096)*self.Length)-self.Length/2
        self.y = self.K_yy*Y + self.y0
        
        return self.y
        
    def ZScan (self):
        '''!@brief 
        '''
        self.X_p = Pin(Pin.cpu.A7, Pin.IN)
        self.X_m = Pin(Pin.cpu.A1, Pin.OUT_PP)
        
        self.Y_p.high()
        self.X_m.low()
        
        self.Y_p = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.Y_m = ADC(Pin(Pin.cpu.A0))
        
        Z_ADC = self.Y_m
        X = Z_ADC.read()
        
        if X < 4000:
            Z = 1
        else:
            Z = 0
        
        return Z
    
    def XYZ_Scan (self):
        '''!@brief 
        '''
        
        
        self.X_p = Pin(Pin.cpu.A7, Pin.OUT_PP)
        self.X_m = Pin(Pin.cpu.A1, Pin.OUT_PP)
        
        self.X_m.low()
        self.X_p.high()
        
        self.Y_p = Pin(Pin.cpu.A6, Pin.IN)
        self.Y_m = ADC(Pin(Pin.cpu.A0))
        

        X_ADC = self.Y_m
        Y_m = X_ADC.read()
        
        self.Y_p = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.Y_m = Pin(Pin.cpu.A0, Pin.OUT_PP)
        
        self.Y_p.high()
        self.Y_m.low()
        
        self.X_p = Pin(Pin.cpu.A7, Pin.IN)
        self.X_m = ADC(Pin(Pin.cpu.A1))
        
        X_ADC = self.X_m
        X_m = X_ADC.read()
        
        
        self.X_p = Pin(Pin.cpu.A7, Pin.IN)
        self.X_m = Pin(Pin.cpu.A1, Pin.OUT_PP)
        
        self.Y_p.high()
        self.X_m.low()
        
        self.Y_p = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.Y_m = ADC(Pin(Pin.cpu.A0))
        
        X = ((Y_m/4096)*self.Width)-self.Width/2
        Y = ((X_m/4096)*self.Length)-self.Length/2
        
        self.Pos_X = self.K_xx*X + self.K_yx*Y + self.x0  
        
        self.Pos_y = self.K_yy*Y + self.K_yx*X + self.y0
        
        Z_ADC = self.Y_m
        X = Z_ADC.read()
        
        if X < 4000:
            self.Z = 1
        else:
            self.Z = 0

      
        return self.Pos_X, self.Pos_y, self.Z
    
    def Speed(self,xyz):
        '''!@brief 
        '''
        posx,posy,z=xyz
        x , y ,z = self.XYZ_Scan()
        Delta_x= (x - posx)
        Delta_y= (y-posy)
        if not z:
            Delta_x = x-posx
            Delta_y = y-posy
        
      
        return Delta_x, Delta_y

    def Calibrate(self):

        Width = 80
        Length = 160
        
        Z=self.ZScan()
        
        print('Touch BRC')
        while Z == False:
            Z=self.ZScan()
        
        utime.sleep(0.4)
        while Z == True:
            Z=self.ZScan()
            x_BRC=self.XScan()
            y_BRC=self.YScan()  
            
        
        utime.sleep(0.4)
        print('Touch BLC')
        while Z == False:
            Z=self.ZScan()
            
        
        utime.sleep(0.4)
        while Z == True:
            Z=self.ZScan()
            x_BLC=self.XScan()
            y_BLC=self.YScan()
        
        utime.sleep(0.4)
        print('Touch Center')
        while Z == False:
            Z=self.ZScan()
            
        
        utime.sleep(0.4)
        while Z == True:
            Z=self.ZScan()
            x_Center=self.XScan()
            y_Center=self.YScan()
        
        utime.sleep(0.4)
        print('Touch TLC')
        while Z == False:
            Z=self.ZScan()
            
        
        utime.sleep(0.4)
        while Z == True:
            Z=self.ZScan()
            x_TLC=self.XScan()
            y_TLC=self.YScan()
            
        utime.sleep(0.4)
        print('Touch TRC')
        while Z == False:
            Z=self.ZScan()
            
        
        utime.sleep(0.4)
        while Z == True:
            Z=self.ZScan()
            x_TRC=self.XScan()
            y_TRC=self.YScan()
        
        print('Finished Calibrating')

        
        self.Mat_A = numpy.array([[-Length/2, Width/2],[Length/2 , Width/2],[0,0],[-Length/2,-Width/2],[Length/2, -Width/2]])
        self.Mat_B = numpy.array([[x_TLC, y_TLC,1],[x_TRC, y_TRC,1],[x_Center, y_Center,1],[x_BLC, y_BLC,1],[x_BRC, y_BRC,1]])
        
        Mat_B_Tran = self.Mat_B.transpose()
        
        C = numpy.dot(Mat_B_Tran,self.Mat_A)
        
        D = numpy.dot(Mat_B_Tran,self.Mat_B)
        
        E = numpy.linalg.inv(D)
        
        Cal_Vars = numpy.dot(E,C)
        
        return Cal_Vars
#        return B
    
    def Set_Cal_Var(self,K_xx,K_xy,x_0,K_yy,K_yx,y_0):
        
        self.K_xx = Cal_Vars[0][0]
        self.K_yx = Cal_Vars[0][1]
        self.K_xy = Cal_Vars[1][0]
        self.K_yy = Cal_Vars[1][1]
        self.x0 = Cal_Vars[2][0]
        self.y0 = Cal_Vars[2][1]
#        print( f"{self.K_xx}, {self.K_yx}, {self.K_xy}, {self.K_yy}, {self.x0}, {self.y0}")
        
  
if __name__ =='__main__':
 import time  
 import pyb 
 x=TouchPad(Pin.cpu.A7,Pin.cpu.A1,Pin.cpu.A6,Pin.cpu.A0,176,100)
# n=0
#
 Cal_Vars=x.Calibrate()
 K_xx = Cal_Vars[0][0]
 K_yx = Cal_Vars[0][1]
 K_xy = Cal_Vars[1][0]
 K_yy = Cal_Vars[1][1]
 x0 = Cal_Vars[2][0]
 y0 = Cal_Vars[2][1]
 x.Set_Cal_Var(K_xx,K_xy,x0,K_yy,K_yx,y0)
#
 while True:
      
      print(x.Speed(x.XYZ_Scan()))

#      print(x.Speed(x,y))
#     print(f'x:{x.XScan()} Cies')
#     print(f'y:{x.YScan()} cookl')
#     print(f'y:{x.zScan()} appl')
